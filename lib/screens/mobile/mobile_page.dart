import 'package:flutter/material.dart';
import 'package:prueba/screens/widgets/contact_info.dart';
import 'package:prueba/screens/widgets/profile_avatar.dart';
import 'package:prueba/screens/widgets/profile_info.dart';
import 'package:prueba/screens/widgets/text_container.dart';

class MobilePage extends StatelessWidget {
  const MobilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: const [
                ProfileAvatar(),
                SizedBox(
                  width: 15,
                ),
                ProfileInfo(),
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            const ContacInfo(),
            const SizedBox(
              height: 15,
            ),
            Container(
              padding: const EdgeInsets.all(5),
              height: 100,
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(12)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  TextContainer(title: "Cursos completados", count: "2/5"),
                  TextContainer(
                      title: "Cursos sincronos y biended", count: "0/2"),
                  TextContainer(title: "Rutas", count: "0/1"),
                  TextContainer(title: "Certificaciones", count: "0/1"),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


