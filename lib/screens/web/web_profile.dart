import 'package:flutter/material.dart';
import 'package:prueba/screens/widgets/contact_info.dart';
import 'package:prueba/screens/widgets/profile_avatar.dart';
import 'package:prueba/screens/widgets/profile_info.dart';

class WebPage extends StatelessWidget {
  const WebPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          SizedBox(
            height: 350,
            child: Container(
              margin: const EdgeInsets.all(40),
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
              height: 300,
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(12),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                  
                    children: [
                    const ProfileAvatar(),
                    SizedBox(height: 10,),
                    Expanded(
                        child: Column(
                      children: const [
                        ProfileInfo(
                          alignment: CrossAxisAlignment.center,
                        ),
                        ContacInfo()
                      ],
                    ))
                  ]),
                  SizedBox(
                    width: 150,
                    height: 45,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                                const Color(0xff2578E3))),
                        onPressed: () {},
                        child: const Text(
                          "Descargar informe",
                          style: TextStyle(fontSize: 14),
                        )),
                  )
                ],
              ),
            ),
          ),
          const CardsItems(),
        ],
      ),
    );
  }
}

class CardsItems extends StatelessWidget {
  const CardsItems({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
        bottom: 0,
        right: 60,
        child: Row(
          children: List.generate(
            4,
            (index) => Container(
              margin: const EdgeInsets.all(5),
              padding: const EdgeInsets.all(5),
              height: 150,
              width: 150,
              child: Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      "Cursos virtuales y externos",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 50),
                      child: Divider(
                        height: 10,
                        thickness: 3,
                        color: Colors.black,
                      ),
                    ),
                    Text("2/5",
                        style: TextStyle(
                            fontSize: 22,
                            color: Color(0xff2578E3),
                            fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
