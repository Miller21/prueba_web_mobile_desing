import 'package:flutter/material.dart';
import 'package:prueba/screens/mobile/mobile_page.dart';
import 'package:prueba/screens/web/web_profile.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: LayoutBuilder(
  builder: (context, constraints) {
    if (constraints.maxWidth > 800) {
      // Mostrar widget para computadoras de escritorio
      return const WebPage();
    } else {
      // Mostrar widget para dispositivos móviles
      return const MobilePage();
    }
  }));
  }
}