
import 'package:flutter/material.dart';

class ContacInfo extends StatelessWidget {
  const ContacInfo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: const [
        SizedBox(
          width: 20,
        ),
        Icon(
          Icons.email,
          color: Colors.black87,
        ),
        SizedBox(
          width: 20,
        ),
        Text(
          "millerhernandez938@gmail.com",
          maxLines: 2,
          style: TextStyle(fontSize: 18, color: Colors.black54),
        ),
      ],
    );
  }
}
