
import 'package:flutter/material.dart';

class ProfileAvatar extends StatelessWidget {
  const ProfileAvatar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
     radius: 45,
     backgroundColor: const Color(0xff16C0B4),
    child: Container(
     margin: const EdgeInsets.all(5),
     decoration: BoxDecoration(
       color: Colors.white,
       borderRadius: BorderRadius.circular(50),
       image: const DecorationImage(image: AssetImage("assets/profile.jpg"))
     ),
    ));
  }
}
