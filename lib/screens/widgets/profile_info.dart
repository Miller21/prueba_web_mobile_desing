
import 'package:flutter/material.dart';

class ProfileInfo extends StatelessWidget {
  const ProfileInfo({
    Key? key,  this.alignment = CrossAxisAlignment.start,
  }) : super(key: key);
  final CrossAxisAlignment alignment;
  

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: alignment,
        children: const [
          Text(
            "Miller Alexander " '\n' "Olaya Hernandez",
            maxLines: 2,
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            "AUDITORÍA",
            style: TextStyle(
                fontSize: 18, color: Colors.black38, letterSpacing: 5),
          ),
        ],
      ),
    );
  }
}
