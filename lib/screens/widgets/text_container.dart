import 'package:flutter/material.dart';

class TextContainer extends StatelessWidget {
  const TextContainer({
    Key? key, required this.title, required this.count,
  }) : super(key: key);
  final String title;
  final String count;

  @override
  Widget build(BuildContext context) {
    return Expanded(
              
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children:  [
          Text(title, textAlign: TextAlign.center, style: const TextStyle(fontSize: 13),),
          Text(count, style: const TextStyle(fontSize: 18, color: Color(0xff2578E3), fontWeight: FontWeight.bold))
        ],
      ),
    );
  }
}